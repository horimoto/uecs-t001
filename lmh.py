#!/usr/bin/python3
# coding:utf-8
#
# Long Message Handler using by IM920[c]
#
# Masafumi Horimoto Holly&Co.,Ltd. (C) 2017-
#
import os
import sys
import getopt
import serial
import time
import operator
import binascii
import fcntl
import termios

# Constant value
PRGNAME="iM920 Long Message Handler"
VERSION="0.11"

# Status value
defttyv="/dev/ttyUSB0"
cmdmode=None
dispresp=False   # 動作応答を表示する

def getkey():
    fno = sys.stdin.fileno()

    #stdinの端末属性を取得
    attr_old = termios.tcgetattr(fno)

    # stdinのエコー無効、カノニカルモード無効
    attr = termios.tcgetattr(fno)
    attr[3] = attr[3] & ~termios.ECHO & ~termios.ICANON # & ~termios.ISIG
    termios.tcsetattr(fno, termios.TCSADRAIN, attr)

    # stdinをNONBLOCKに設定
    fcntl_old = fcntl.fcntl(fno, fcntl.F_GETFL)
    fcntl.fcntl(fno, fcntl.F_SETFL, fcntl_old | os.O_NONBLOCK)
    chr = 0
    try:
        # キーを取得
        ck = sys.stdin.read(1)
        if len(ck):
            chr = ord(ck)
#            while len(ck):
#                chr = (chr << 8) + ord(ck)
#                ck = sys.stdin.read(1)
    finally:
        # stdinを元に戻す
        fcntl.fcntl(fno, fcntl.F_SETFL, fcntl_old)
        termios.tcsetattr(fno, termios.TCSANOW, attr_old)
    return chr

def usage():
    print("=== {0} ===".format(PRGNAME))
    print(" Options")
    print("   -V, --version      : Display version")
    print("   -h, --help         : Display help")
    print("   -t, --term ttyname : Set tty device\n")


class LMHIM920:
    ##
    # 初期設定
    # @param なし
    # @return なし
    #
    def __init__(self, comPort = defttyv, timeout = 0):
        self.ser = serial.Serial(port=comPort, 
                                 baudrate=19200, 
                                 bytesize=8,
                                 parity='N',
                                 stopbits=1,
                                 xonxoff=0,
                                 timeout=timeout,
                                 writeTimeout=None)
        self.ser.flushInput()
        print(self.ser.name)
    ##
    # ポートクローズ
    #
    def close(self):
        self.ser.close()

    ##
    # 指定長さのデータを読み込む
    # @return データパケット
    #
    def read(self):
        packet = ''
        length = 0
        tout = 0
        cmax = 0
        try:
            while True:
                rsize = self.ser.inWaiting()
                if rsize > 0:
                    c = self.ser.read(rsize)
                    if (rsize==4) and (c==b"OK\r\n"):
                        return "OK"
                    if (rsize==4) and (c==b"NG\r\n"):
                        return "NG"
                    #print("1 RSIZE=",rsize,"   TXT=",c.decode())
                    packet += c.decode()
                    #print("PACKET=",packet,"  C=",c)
                    lastc = c[rsize-1]
                    if int(lastc) == 10:
                        #print("2 RSIZE=",rsize,"  TXT=",packet)
                        break
                else:
                    if cmax>10:
                        break
                    time.sleep(0.01)
                    cmax += 1
            #
            # 1レコード読み終わったのでデータを復号する
            #
            if packet!="":
                if cmdmode!=None:
                    print(packet)
                    return None
                pd = packet.split(':')
                bd = ""
                for v in pd[1].split(','):
                    #print(v)
                    pd2 = int(v,16)
                    y   = chr(pd2)
                    bd += str(y)
#                print(bd)
                return bd
        except:
            return None

    ##
    # 電文を送信し指定長さのデータを受け取る
    # @param packet   電文長
    # @return 受信電文
    #
    def send(self, packet, t="data"):
        if t=="data":
            u = "TXDA "
            for v in packet:
                u += hex(ord(v))[2:4]
            if dispresp:
                print("SEND:", u)
        else:
            u = packet[1:]
            #print(u)
        if self.ser.writable():
            self.ser.write(bytes(u,'UTF-8'))
            self.ser.write(b"\r\n")
        return None


if __name__ == '__main__':
    try:
        opts,args = getopt.getopt(sys.argv[1:],
                                  'Vht:',
                                  ['version','help','term='])
    except getopt.GetoptError as err:
        print("missing options.")
        print(str(err))
        sys.exit(0)

    for o,a in opts:
        if o in ("-V","--version"):
            print("{0} Version:{1}".format(PRGNAME,VERSION))
            sys.exit(0)
        elif o in ("-h","--help"):
            usage()
            sys.exit(0)
        elif o in ("-t","--term"):
            defttyv="/dev/"+a
    try:
        im920 = LMHIM920(defttyv, 0.1)
        txt = ""
        while True:
            key = getkey()
            if int(key)==10: # LF
                if txt[0:1]=='=':
                    cmdmode = txt[1:]
                    if cmdmode=="response":
                        dispresp = True
                        cmdmode  = None
                        txt = ""
                        key = 0
                    elif cmdmode=="noresponse":
                        dispresp = False
                        cmdmode  = None
                        txt = ""
                        key = 0
                    else:
                        im920.send(txt,"order")
                else:
                    cmdmode = None
                    im920.send(txt)
                txt=""
                key=0
            if int(key)!=0:
                txt += str(chr(key))
            a = im920.read()
            if a != None:
                print(a,)
                if a=='A':
                    print("END")
                    sys.exit()
            else:
                time.sleep(0.1)

    except KeyboardInterrupt:
        sys.exit()

# test data
# TXDA 4142434445464748494a4b4c4d4e4f505152535455565758595a30313233343536373839

