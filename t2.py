#! /usr/bin/python3

x="02,55C9,BD:30,31,30,32,30,33,30,34,30,35,30,36,30,37,30,38,30,39,31,30,31,31,31,32,31,33,31,34,31,35,31,36,31,37,31,38,31,39,32,30,32,31,32,32,32,33,32,34,32,35,32,36,32,37,32,38,32,39,33,30,33,31,33,32"
x1=x.split(':')
b = ""
for v in x1[1].split(','):
    x2 = int(v,16)
    y  = chr(x2)
    b += y

print(b)

import fcntl
import termios
import sys
import os

def getkey():
    fno = sys.stdin.fileno()

    #stdinの端末属性を取得
    attr_old = termios.tcgetattr(fno)

    # stdinのエコー無効、カノニカルモード無効
    attr = termios.tcgetattr(fno)
    attr[3] = attr[3] & ~termios.ECHO & ~termios.ICANON # & ~termios.ISIG
    termios.tcsetattr(fno, termios.TCSADRAIN, attr)

    # stdinをNONBLOCKに設定
    fcntl_old = fcntl.fcntl(fno, fcntl.F_GETFL)
    fcntl.fcntl(fno, fcntl.F_SETFL, fcntl_old | os.O_NONBLOCK)

    chr = 0

    try:
        # キーを取得
        c = sys.stdin.read(1)
        if len(c):
            while len(c):
                chr = (chr << 8) + ord(c)
                c = sys.stdin.read(1)
    finally:
        # stdinを元に戻す
        fcntl.fcntl(fno, fcntl.F_SETFL, fcntl_old)
        termios.tcsetattr(fno, termios.TCSANOW, attr_old)

    return chr

if __name__ == "__main__":
    while 1:
        key = getkey()
        # enterで終了、キー入力があれば表示
        if key == 10:
            break
        elif key:
            print(chr(key))
