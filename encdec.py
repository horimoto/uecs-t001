#! /usr/bin/python3
# coding:utf-8

def txtdec(t):
    ot = ""
    for cs in t.split(','):
        cv = int(cs,16)
        ot+= chr(cv)
    return(ot)

def txtenc(t):
    ot = ""
    for cs in list(t):
        ot += hex(ord(cs))[2:4]
    return(ot)

def txtsep(t):
    ot = ""
    cv = t.split(':')
    cx = cv[0].split(',')
    cy = cv[1]
    print("NODE=",cx[0],"   MOD_ID=",cx[1],"  RSSI=",cx[2])
    cz = txtdec(cy)
    print(cz)

    
if __name__ == '__main__':
    m = "DEF TXTENC"
    n = txtenc(m)
    print(n)
    
    x = "50,51,52,53,54,55,56,41,42,43,44,45,46,47,48,49"
    y = txtdec(x)
    print(y)

    x = "AB,0001,7C:43,57,6B,4F,42,46,5A,5E"
    txtsep(x)
    
