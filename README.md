# UECS用iM920トライアルプログラム

	Version 0.03  
	horimoto-at-holly-linux.com

64バイトを越える長文を伝送するプログラム

## 1. プログラムの流れ

 平文の普通の入力を行うと、IM920のバイナリ電文に変換してIM920に渡し送信する。
 
 受信すると、IM920のバイナリ電文を人間のわかるアルファベットの羅列に戻して表示する。
 
 受信に関しては、若干のディレイはあるものの非同期な着信を可能としている。
 したがって、単純なチャットプログラムにもなる。
 
 lmh.py プログラムを単体で起動すると。チャットプログラムになる。
 
 
## 2. 対応機種と接続例

Python3が動いて、USBなどのシリアル接続でiM920と接続できればPCでもRaspberryPiやOrangePiなどでも動作可能。

RaspberryPiやOrangePiなどとの接続を行う場合には、特にRaspberryPiの場合にはUSBインタフェースが有り余っているので、Fig-1のようにUSBシリアルコンバータを介すると良い。

![RPi/OPi接続例](images/fig-1.jpg "Fig-1.RPi/OPi接続例")

OPiの場合には、USBポートが1個だけだが、ttyS?としてUARTが4個用意されているので、これを使うと3.3Vでインタフェース回路が不要となり、直結出来るので効率的である。

![OPiのttySを用いる例](images/fig-2.jpg "Fig-2.OPi直結例")

一般的なPCを用いる場合には、USBシリアル変換アダプタを介する。

![PCを接続する例](images/fig-3.jpg "Fig-3.PC接続例")

## 3. コマンド実行例

実行に際してはsudoを前置してroot権限で実行する必要がある場合がある。
シリアルインタフェースへのアクセス権のためである。

    $ sudo lmh.py [options]
	    options:
		    -v,--version  versionの表示
			-h,--help     helpの表示
			-t,--term ttyname  ttyデバイス名(標準はttyUSB0)
			
## 4. Classとしての使用例

準備中(Look sources)


